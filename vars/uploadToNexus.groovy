def call(paramsMap){
	 def pomfile = readMavenPom file: 'pom.xml';
     def artifactId = pomfile.artifactId
     def type = pomfile.packaging
     def groupId = pomfile.groupId
     def version = pomfile.version
     def repository = paramsMap.nexusRepoName
     echo "######    target/${artifactId}-${version}.${type}     ######"
     echo "######    target/$artifactId-$version.$type     ######"
     nexusArtifactUploader artifacts: [[artifactId: artifactId, classifier: '', file: "target/${artifactId}-${version}.${type}", type: type]], 
	 credentialsId: 'nexus3creds', groupId: groupId, nexusUrl: '13.211.128.175:8081', nexusVersion: 'nexus3', 
	 protocol: 'http', repository: repository, version: version
}